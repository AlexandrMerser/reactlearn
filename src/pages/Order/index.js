import React, { useState } from "react";

import Form from "react-bootstrap/Form";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Badge from "react-bootstrap/Badge";

import cart from "~s/cart";

export default ({
  formData,
  onChangeValue,
  moveToCart,
  moveToResult,
  clearForm
}) => {
  const [openModal, setOpenModal] = useState(false);

  const renderForm = data => {
    const arrayForm = [];

    for (let name in data) {
      const field = data[name];

      arrayForm.push(
        <Form.Group key={name} controlId={`formGroup${name}`}>
          <Form.Label>{field.label}</Form.Label>
          <Form.Control
            type={field.type}
            placeholder={field.placeholder}
            onChange={e => onChangeValue(name, e.target.value)}
            value={field.value}
          />
        </Form.Group>
      );
    }

    return arrayForm;
  };

  return (
    <>
      <Form style={{ maxWidth: "400px", margin: "0 auto" }}>
        {renderForm(formData)}

        <ButtonToolbar>
          <Button variant={"outline-warning"} onClick={e => moveToCart(e)}>
            Назад к корзине
          </Button>
          <Button
            variant={"outline-secondary"}
            style={{ marginLeft: "10px" }}
            onClick={() => setOpenModal(true)}
          >
            Подтвердить
          </Button>
        </ButtonToolbar>
      </Form>

      <Modal show={openModal} onHide={() => {}}>
        <Modal.Header closeButton={true} onHide={() => setOpenModal(false)}>
          <Modal.Title>Подтверждение заказа</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>
            {formData.name.value !== "" ? (
              <Badge variant="danger">{formData.name.value + ", "}</Badge>
            ) : (
              ""
            )}{" "}
            Вы уверены, что хотите подтвердить заказ? Сумма вашей покупки
            составит - <Badge variant="info">{cart.total}</Badge> Рублей
          </h5>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => {
              setOpenModal(false);
              moveToCart();
              clearForm();
            }}
          >
            Отменить
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              setOpenModal(false);
              moveToResult();
            }}
          >
            Подтвердить
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
