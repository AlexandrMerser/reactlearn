import React, { useState, useEffect, useReducer } from "react";

import AppMinMax from "~c/Counter";

import cart from "~s/cart";

import { observer } from "mobx-react";

const Cart = observer(({ moveToOrder }) => {
  const productsList = cart.products.map((product, index) => (
    <tr key={product.id}>
      <th>{product.title}</th>
      <td>{product.price}</td>
      <td>
        <AppMinMax
          min={1}
          max={product.rest}
          cnt={product.current}
          onChange={cnt => {
            cart.change(index, cnt);
          }}
        />
      </td>
      <td>
        {product.price * product.current}
        <button
          type="button"
          className="close"
          aria-label="Close"
          onClick={() => cart.remove(index)}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </td>
    </tr>
  ));

  return (
    <>
      <h3>Корзина</h3>
      <table className="table table-striped">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Кол-во</th>
            <th scope="col">Сумма</th>
          </tr>
        </thead>
        <tbody>
          {productsList}
          <tr>
            <th colSpan="3">Общая сумма</th>
            <td>{cart.total}</td>
          </tr>
        </tbody>
      </table>
      <hr />
      <button onClick={() => moveToOrder()} className="btn btn-success">
        Перейти к оформлению заказа...
      </button>
    </>
  );
});

export default Cart;
