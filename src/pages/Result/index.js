import React from "react";

import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";

import cart from "~s/cart";

export default ({ formData, moveToCart, clearForm }) => {
  return (
    <>
      <h1>
        <Badge variant="primary">{formData.name.value}</Badge>, спасибо за
        покупку
      </h1>
      <h2>
        Сумма вашего заказа составила -{" "}
        <Badge variant="success">{cart.total}</Badge> рублей
      </h2>
      <h3>
        Письмо с подтверждением отправлено на ваш адрес -{" "}
        <a href={`mailto:${formData.email.value}`}>{formData.email.value}</a>
      </h3>

      <Button
        variant="outline-success"
        onClick={() => {
          moveToCart();
          clearForm();
        }}
      >
        Назад к корзине
      </Button>
    </>
  );
};
