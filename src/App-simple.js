import React from "react";

import InputLazy from "./components/Input/lazy";

export default class extends React.Component {
  state = {
    inp1: "",
    inp2: ""
  };

  render() {
    return (
      <div>
        <h2>Lazy Input</h2>
        <p> {this.state.inp1}</p>
        <InputLazy
          nativeProps={{ type: "text" }}
          value={this.state.inp1}
          onChange={e => this.setState({ inp1: e.target.value })}
        />
        <button onClick={e => this.setState({ inp1: "test" })}>
          Unreal Change
        </button>
      </div>
    );
  }
}
