import React, { useEffect } from "react";

export default function({ value, nativeProps, onChange }) {
  const inputRef = React.createRef();

  useEffect(() => {
    inputRef.current.value = value;
  }, [value]);

  const checkChange = e => {
    if (value.toString() !== e.target.value) {
      onChange(e);
    }
  };

  const checkEnterKey = e => {
    if (e.keyCode === 13) {
      checkChange(e);
    }
  };

  return (
    <input
      {...nativeProps}
      defaultValue={value}
      onBlur={checkChange}
      onKeyUp={checkEnterKey}
      ref={inputRef}
    />
  );
}
