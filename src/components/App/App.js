import React, { useState, useEffect } from "react";

import Cart from "~/pages/Cart";
import Order from "~/pages/Order";
import Result from "~/pages/Result";

export default function() {
  const [formData, setFormData] = useState({
    name: {
      label: "Имя",
      type: "text",
      placeholder: "Введите Ваше имя",
      value: ""
    },
    email: {
      label: "Почта",
      type: "email",
      placeholder: "Введите Вашу почту",
      value: ""
    },
    phone: {
      label: "Телефон",
      type: "tel",
      placeholder: "Введите Ваш телефон",
      value: ""
    }
  });

  const onChangeValue = (name, value) => {
    let newFormData = { ...formData };
    newFormData[name] = { ...newFormData[name], value };

    setFormData(newFormData);
  };

  const clearForm = () => {
    let newFormData = { ...formData };
    for (let name in newFormData) {
      newFormData[name] = { ...newFormData[name], value: "" };
    }

    setFormData(newFormData);
  };

  const [activeRout, setActiveRout] = useState("CART");

  const moveToCart = () => setActiveRout("CART");
  const moveToOrder = () => setActiveRout("ORDER");
  const moveToResult = () => setActiveRout("RESULT");

  let page;

  switch (activeRout) {
    case "CART":
      page = <Cart moveToOrder={moveToOrder} />;
      break;

    case "ORDER":
      page = (
        <Order
          formData={formData}
          onChangeValue={onChangeValue}
          moveToCart={moveToCart}
          moveToResult={moveToResult}
          clearForm={clearForm}
        />
      );
      break;

    case "RESULT":
      page = (
        <Result
          moveToCart={moveToCart}
          moveToResult={moveToResult}
          formData={formData}
          clearForm={clearForm}
        />
      );
      break;

    default:
      page = <div>ERROR 404</div>;
  }

  return <div className="container-sm wrapper-outter">{page}</div>;
}
