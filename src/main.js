import React from "react";
import ReactDom from "react-dom";
import App from "./components/App/App";
// import AppSimple from "./App-simple";

ReactDom.render(<App />, document.querySelector("#app"));
