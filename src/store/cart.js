import { observable, computed, action } from "mobx";

class Cart {
  @observable products = getProduct();

  @computed get total() {
    return this.products.reduce(
      (acc, product) => acc + product.price * product.current,
      0
    );
  }

  @action change(i, cnt) {
    this.products[i].current = cnt;
  }

  @action remove(i) {
    this.products.splice(i, 1);
  }
}

export default new Cart();

function getProduct() {
  return [
    {
      id: 100,
      title: "Iphone 200",
      price: 12000,
      rest: 10,
      current: 1
    },
    {
      id: 101,
      title: "Samsung AAZ8",
      price: 22000,
      rest: 432,
      current: 1
    },
    {
      id: 102,
      title: "Витязь ЭР600",
      price: 32231,
      rest: 15,
      current: 1
    },
    {
      id: 103,
      title: "Sony XPeria",
      price: 32231,
      rest: 124,
      current: 1
    },
    {
      id: 104,
      title: "Honor 20",
      price: 32231,
      rest: 42,
      current: 1
    }
  ];
}
